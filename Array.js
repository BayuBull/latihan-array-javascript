var namaArray=['bayu','agung','nugroho',60,false];

console.log(namaArray);

// menghitung panjang array
console.log((namaArray.length));
// mengganti array
namaArray[2]='cahya';
console.log(namaArray);

//method array
//Push
namaArray.push('bulldog');
//unshift
namaArray.unshift(70);
console.log(namaArray);
//Pop
namaArray.pop();
console.log(namaArray);
// indexOf
console.log(namaArray.indexOf('agung'));

// CONTOH SOAL //

/*John pergi berlibur dan pergi ke 3 restoran berbeda. Tagihan ini berisi $ 124, $ 48 dan $ 258.
john tips pelayan jumlah yang adil, john membuat kalkulator tip sederhana (sebagai fungsi).
dia suka tip 20% kurang dari $50, 15% untuk $50 sampai $ 200  10% jika tagihan lebih dari $ 200
pada akhir, john ingin memiliki 2 array:
1) berisi semua tip kamu (satu untuk setiap tagihan)
2) berisi semua jumlah pembayaran final (tagihan + kiat)

(catatan: untuk menghitung 20% dari nilai, gandakan sederhana jika dengan 20/100 = 0,2) */
//fungsi
/*function calculator(bill){
    var persentase;
    if(bill < 50){
        persentase = .2;
    }else if(bill >=50 && bill < 200){
        persentase =.15;
    }else {
        persentase = .1;
    }
    return persentase * bill;
}

console.log(calculator(200))
*/
function calculator(bill){
    var persentase;
    if(bill < 50){
        persentase = .2;
    }else if(bill >=50 && bill < 200){
        persentase =.15;
    }else {
        persentase = .1;
    }
    return persentase * bill;
}
var bill=[124,48,268];
var tip =[calculator(bill[0]),calculator(bill[1]),calculator(bill[2])];
var total =[bill[0] + tip[0],bill[1] + tip[1],bill[2] + tip[2]];
console.log(tip,total);
