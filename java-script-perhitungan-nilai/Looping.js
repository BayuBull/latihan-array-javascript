//Looping 1 bilangan
// for(i=0; i<10; i++){
//     console.log('looping 1 bilangan'+' '+i)
// }

//Looping 2 bilangan
// for(i=0; i<10; i+=2){
//     console.log('looping 2 bilangan'+' '+ i)
// }

//Looping Array
// var data =['bayu',60,'agung',true];
// for(var i=0; i<data.length; i++){
//     console.log(data[i])
// }

//Looping Array Continue and break
var data =['bayu',60,'agung',true];
for(var i =0; i<data.length; i++){
    if(typeof data[i] !== "string" ) continue;
    console.log('data continue'+' '+ data[i])
}
var data =['bayu',60,'agung',true];
for(var i =0; i<data.length; i++){
    if(typeof data[i] !== "string" ) break;
    console.log('data break'+' '+data[i])
}

// looping dari belakang dengan for
for(var i= data.length - 1; i >=0 ; i-- ){
    console.log(data[i])
}


// looping objek dan fungsi
/*
menghitung tagiahan dari bill restoran dan tips
*/

var bayu={
    fullname:'bayu agung ',
    bills:[124,48,268,42],
    calkulator: function(){
        //membuat array kosong
        this.tips=[];
        this.total=[];

        for(var i=0; i < this.bills.length; i++)
        {
            //rule persentase
            var persen;
            var bill = this.bills[i];

            if (bill < 50){
                persen= .20;
            }else if (bill >= 50 && bill < 200){
                persen= .15;
            }else{
                persen= .10;
            }
            this.tips[i]= bill * persen;
            this.total[i]= bill+bill*persen;
        }
    }
}
bayu.calkulator();
console.log(bayu);

var kelasA={
    nilai:[9,85,75,7,8,55,64],
    hitungnilai : function(){

        this.fixNilai=[]; 

        for( i=0; i < this.nilai.length; i++)
    {
        var penilaian;
        var n = this.nilai[i];

        if (n < 85){
            penilaian="A";
        }else if(n > 85 && n <= 75){
            penilaian="B";
        }else if(n>75 && n <= 65){
            penilaian="C";
        }else{
            penilaian="D";
        }
        this.fixNilai[i]= n == penilaian;
    }
    
    }
}
kelasA.hitungnilai();
console.log(kelasA);